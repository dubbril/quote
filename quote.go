package quote

// Return : Hello
func Say() string {
	return "Hello"
}

// Return : Hi, mate
func Speak() string {
	return "Hi, mate"
}
